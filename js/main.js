// Main js

$(document).ready(function(){


    //Slider Banner
    $('.js-banner-slider').slick({
        infinite:true,
        slidesToShow:1,
        slidesToScroll:1,
        dots:false,
        arrows:true,
        autoplay:false,
        prevArrow:$('.js-arrow-prev'),
        nextArrow:$('.js-arrow-next')
    });
    //Slider Intro
    $('.js-intro-slider').slick({
        infinite:true,
        slidesToShow:1,
        slidesToScroll:1,
        dots:false,
        arrows:true,
        autoplay:false,
        prevArrow:$('.js-intro-arrow-prev'),
        nextArrow:$('.js-intro-arrow-next')
        
    });
    //Slider Gallery
    $('.js-gallery-slider').slick({
        infinite:true,
        slidesToShow:1,
        slidesToScroll:1,
        dots:false,
        arrows:true,
        autoplay:false,
        prevArrow:$('.js-gallery-arrow-prev'),
        nextArrow:$('.js-gallery-arrow-next')
        
    });
});